import {iterable, iterator} from "./promise.types.js";

export async function each(iterable: iterable, cb: iterator){
    for (const item of iterable) {
        await cb(item);
    }
    return iterable;
}

