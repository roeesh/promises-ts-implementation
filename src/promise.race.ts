export async function race(promises: any[] | Promise<any>[]) {
    return await new Promise((resolve)=>{
        promises.forEach(async p => {
            resolve(await p);
        });
    });
}