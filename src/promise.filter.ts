import {iterable, iterator} from "./promise.types.js";

//--------------------------------------------------

export async function filterSeries(iterable: iterable, cb: iterator): Promise<iterable>{
    const results = [];
    for (const item of iterable) {
        if(await cb(item) === true) results.push(item);
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------

export async function filterParallel(iterable: iterable, cb: iterator): Promise<iterable>{

    const results = [];
    const pending = Array.from(iterable, item => cb(item));

    for (const [i,p] of pending.entries()) {
        if(await p === true) results.push(iterable[i]);
    }
    
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
