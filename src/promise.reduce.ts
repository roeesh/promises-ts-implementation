import {iterable, reducer} from "./promise.types.js";


export async function reduce(iterable: iterable, cb: reducer, initial?: any): Promise<iterable>{

    let aggregator = initial !== undefined ? initial : iterable[0];

    for (const item of iterable) {
        aggregator = await cb(aggregator,item);
    }
    
    return aggregator;
}

