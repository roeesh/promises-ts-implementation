export type iterable = Promise<any>[] | any[] | string;
export type iterator = (value: any | Promise<any>| string, index?: number, arrayLength?: number) => Promise<any>|any|string;
export type reducer = (accumulator: any | iterable, item: any|Promise<any>, index?: number, length?: number) => Promise<any>|any|string;
export interface IPromiseObject {
    [key:string]: Promise<any>;
}