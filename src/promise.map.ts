
import {iterable, iterator} from "./promise.types.js";

//--------------------------------------------------

export async function mapParallel(iterable: iterable, cb: iterator): Promise<any[]>{
    const results = [];
    const pending = Array.from(iterable, item => cb(item));
    for (const p of pending) {
        results.push(await p); 
    }
    return results;
}

//--------------------------------------------------

export async function mapSeries(iterable: iterable, cb: iterator){
    const results = [];
    for (const item of iterable) {
        results.push(await cb(item));
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
