interface IPromiseObject {
    [key:string]: Promise<any>;
}

export async function props(promisesObj: IPromiseObject) : Promise<IPromiseObject> {
    const results = {} as IPromiseObject;
    for (const key in promisesObj) {
      results[key] = await promisesObj[key];
    }  
    return results;
  }