export async function some(promises: any[] | Promise<any>[] ,num: number) {
    const results = [] as any[];
    return await new Promise((resolve)=>{
        promises.forEach(async p => {
            results.push(await p);
            if(results.length === num) resolve(results); 
        });
    });
}