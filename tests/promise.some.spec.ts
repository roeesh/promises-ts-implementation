import { expect } from "chai";
import { some } from "../src/promise.some";
import { echo } from "../src/promise.utils";

describe("The promise.some module", ()=>{

    context(`#some`, ()=> {
        let pendings = [] as Promise<any>[];
        before(()=> {
            const promise1 = echo("first",4000);
            const promise2 = echo("second",400);
            const promise3 = echo("third",3000);
            const promise4 = echo("forth",3000);
            const promise5 = echo("fifth",500);
            pendings = [promise1,promise2,promise3,promise4,promise5];
            
        });

        it(`should exist`, ()=> {
            expect(some).to.be.a("function");
            expect(some).to.be.instanceOf(Function);
        });

        it(`should return the first n resolved promises`,async ()=> {
            const actual = await some(pendings,2);
            expect(actual).to.eql(["second","fifth"]);
        });
    });
});