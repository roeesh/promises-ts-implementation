import { expect } from "chai";
import { all } from "../src/promise.all";
import { echo } from "../src/promise.utils";

describe("The promise.all module", ()=>{

    context(`#all`, ()=> {
        let pendings = [] as Promise<any>[];
        before(()=> {
            const promise1 = echo("1 first resolved value", 3000);
            const promise2 = echo("2 second resolved value", 3000);
            const promise3 = echo("3 third resolved value", 3000);
            pendings = [promise1,promise2,promise3];
            
        });

        it(`should exist`, ()=> {
            expect(all).to.be.a("function");
            expect(all).to.be.instanceOf(Function);
        });

        it(`should return a promise that resolves to an array of the results of the input promises`,async ()=> {
            const actual = await all(pendings);
            expect(actual).to.eql([
                '1 first resolved value',
                '2 second resolved value',
                '3 third resolved value'
              ]);
        });
    });
});