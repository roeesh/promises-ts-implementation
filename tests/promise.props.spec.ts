import { expect } from "chai";
import { props } from "../src/promise.props";
import { echo } from "../src/promise.utils";
import { IPromiseObject } from "../src/promise.types";

describe("The promise.props module", ()=>{

    context(`#props`, ()=> {
        let pendings = {} as IPromiseObject;
        before(()=> {
            const promise1 = echo("1 first resolved value", 3000);
            const promise2 = echo("2 second resolved value", 3000);
            const promise3 = echo("3 third resolved value", 3000);
            pendings = {promise1,promise2,promise3};
            
        });

        it(`should exist`, ()=> {
            expect(props).to.be.a("function");
            expect(props).to.be.instanceOf(Function);
        });

        it(`should return a promise that resolves to an object of the results of the input promises`,async ()=> {
            const actual = await props(pendings);
            expect(actual).to.eql({
                promise1: '1 first resolved value',
                promise2: '2 second resolved value',
                promise3: '3 third resolved value'
              });
        });
    });
});