import { expect } from "chai";
import { each } from "../src/promise.each";
import { random, delay } from "../src/promise.utils";
import log from "@ajar/marker";


describe("The promise.each module", ()=>{

    context(`#each`, ()=> {
        
        it(`should exist`, ()=> {
            expect(each).to.be.a("function");
            expect(each).to.be.instanceOf(Function);
        });

        it(`should return the input resolved`,async ()=> {
            const actual = await each("Geronimo",async char => {
                log.cyan(`${char} -->`);
                await delay( random(1000,500) );
                log.magenta(`<-- ${char}`);
                return char.toUpperCase();
            });
            expect(actual).to.equal("Geronimo");
        });
    });
});