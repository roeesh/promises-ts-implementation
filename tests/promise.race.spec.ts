import { expect } from "chai";
import { race } from "../src/promise.race";
import { echo } from "../src/promise.utils";

describe("The promise.race module", ()=>{

    context(`#race`, ()=> {
        let pendings = [] as Promise<any>[];
        before(()=> {
            const promise1 = echo("first",4000);
            const promise2 = echo("second",1000);
            const promise3 = echo("third",3000);
            pendings = [promise1,promise2,promise3];
            
        });

        it(`should exist`, ()=> {
            expect(race).to.be.a("function");
            expect(race).to.be.instanceOf(Function);
        });

        it(`should return a the first resolved promise`,async ()=> {
            const actual = await race(pendings);
            expect(actual).to.equal("second");
        });
    });
});