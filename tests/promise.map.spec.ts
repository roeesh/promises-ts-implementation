import { expect } from "chai";
import { mapParallel, mapSeries } from "../src/promise.map";
import { random, delay } from "../src/promise.utils";
import log from "@ajar/marker";


describe("The promise.map module", ()=>{

    context(`#mapParallel`, ()=> {
        
        it(`should exist`, ()=> {
            expect(mapParallel).to.be.a("function");
            expect(mapParallel).to.be.instanceOf(Function);
        });

        it(`should return the resolved value`,async ()=> {
            const actual = await mapParallel("Geronimo",async char => {
                log.cyan(`${char} -->`);
                await delay(random(1000,500) );
                log.magenta(`<-- ${char}`);
                return char.toUpperCase(); // Modify each item in the iterable
            });
            expect(actual).to.eql(["G","E","R","O","N","I","M","O"]);
        });
    });

    context(`#mapSeries`, ()=> {
        
        it(`should exist`, ()=> {
            expect(mapSeries).to.be.a("function");
            expect(mapSeries).to.be.instanceOf(Function);
        });

        it(`should return the resolved value`,async ()=> {
            const actual = await mapSeries("Geronimo",async char => {
                log.cyan(`${char} -->`);
                await delay(random(1000,500) );
                log.magenta(`<-- ${char}`);
                return char.toUpperCase();
            });
            expect(actual).to.equal("GERONIMO");
        });
    });
});