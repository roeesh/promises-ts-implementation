import { expect } from "chai";
import { reduce } from "../src/promise.reduce";
import { random, delay } from "../src/promise.utils";
import log from "@ajar/marker";


describe("The promise.reduce module", ()=>{

    context(`#reduce`, ()=> {
        
        it(`should exist`, ()=> {
            expect(reduce).to.be.a("function");
            expect(reduce).to.be.instanceOf(Function);
        });

        it(`should return the resolved value`,async ()=> {
            const actual = await reduce([51,64,25,12,93], async (total,num) => {
                log.cyan(`${num} -->`);
                await delay( random(1000,100) );
                log.magenta(`<-- ${num}`);
                return total + num;
            },0);
            expect(actual).to.equal(245);
        });
    });
});