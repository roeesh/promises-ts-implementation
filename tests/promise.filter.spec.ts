import { expect } from "chai";
import { filterParallel, filterSeries } from "../src/promise.filter";
import { random, delay } from "../src/promise.utils";
import log from "@ajar/marker";


describe("The promise.filter module", ()=>{

    context(`#filterParallel`, ()=> {
        
        it(`should exist`, ()=> {
            expect(filterParallel).to.be.a("function");
            expect(filterParallel).to.be.instanceOf(Function);
        });

        it(`should return the resolved value`,async ()=> {
            const actual = await filterParallel("G<4!e3ro0ni1mo", async char => {
                log.cyan(`${char} -->`);
                await delay( random(1000,500) );
                log.magenta(`<-- ${char}`);
                return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
            });
            expect(actual).to.equal("Geronimo");
        });
    });

    context(`#filterSeries`, ()=> {
        
        it(`should exist`, ()=> {
            expect(filterSeries).to.be.a("function");
            expect(filterSeries).to.be.instanceOf(Function);
        });

        it(`should return the resolved value`,async ()=> {
            const actual = await filterSeries("G<4!e3ro0ni1mo", async char => {
                log.cyan(`${char} -->`);
                await delay( random(1000,100) );
                log.magenta(`<-- ${char}`);
                return /^[A-Za-z]+$/.test(char);
            });
            expect(actual).to.equal("Geronimo");
        });
    });
});