import { iterable } from "./promise.types.js";
export declare function all(promises: iterable): Promise<any[]>;
