import { iterable, reducer } from "./promise.types.js";
export declare function reduce(iterable: iterable, cb: reducer, initial?: any): Promise<iterable>;
