export declare type iterable = Promise<any>[] | any[] | string;
export declare type iterator = (value: any | Promise<any> | string, index?: number, arrayLength?: number) => Promise<any> | any | string;
export declare type reducer = (accumulator: any | iterable, item: any | Promise<any>, index?: number, length?: number) => Promise<any> | any | string;
export interface IPromiseObject {
    [key: string]: Promise<any>;
}
