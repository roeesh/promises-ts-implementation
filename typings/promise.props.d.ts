interface IPromiseObject {
    [key: string]: Promise<any>;
}
export declare function props(promisesObj: IPromiseObject): Promise<IPromiseObject>;
export {};
