import { iterable, iterator } from "./promise.types.js";
export declare function mapParallel(iterable: iterable, cb: iterator): Promise<any[]>;
export declare function mapSeries(iterable: iterable, cb: iterator): Promise<string | any[]>;
