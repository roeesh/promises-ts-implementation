import { iterable, iterator } from "./promise.types.js";
export declare function each(iterable: iterable, cb: iterator): Promise<iterable>;
