import { iterable, iterator } from "./promise.types.js";
export declare function filterSeries(iterable: iterable, cb: iterator): Promise<iterable>;
export declare function filterParallel(iterable: iterable, cb: iterator): Promise<iterable>;
